As of CERNLIB version 2006, upstream ships all CERNLIB code in a single
enormous tarball.  The tarball may be obtained as of this writing from the URL
http://cernlib.web.cern.ch/cernlib/download/2006_source/tar/2006_src.tar.gz

For reasons that are partly historical (this is the first time upstream has
shipped a single monolithic tarball), and partly concerned with maintainability
(having several fairly independent source packages implies less frequent
uploads for each), CERNLIB is split up into four source packages in Debian:
"cernlib", "mclibs", "paw", and "geant321", and these source packages (tarballs
within tarballs) are generated in a somewhat convoluted fashion.  Note that
repackaging the upstream source would have been necessary in any event, due to
numerous unimportant files, as well as some Monte Carlo libraries, that have
unclear or non-free licenses.

In order to generate the orig.tar.gz, one needs to take the following steps.
These instructions are generically applicable to all four CERNLIB source
packages in Debian; everywhere below, consider "cernlib" as a variable standing
for the actual name of the source package in question.

Either:

a) Taking care to have the cernlib-base-dev binary package installed (except
   if you are building the orig.tar.gz for the "cernlib" source package),
   with version >= 2006.dfsg.2-1, cd into the Debian source package directory
   (cernlib-<version> for instance), place the above-mentioned CERNLIB
   2006_src.tar.gz upstream tarball into "..", and run the command
   "fakeroot debian/rules get-orig-source".  The file
   "../cernlib_<version>.orig.tar.gz" (e.g.) should be generated automatically.

b) If you are a glutton for punishment, a new orig.tar.gz can be generated
   manually as follows.  This is best done in a separate scratch directory
   that initially contains no files other than the 2006_src.tar.gz tarball
   from upstream.  You will need to have a copy of the Debian source package
   in question (at least, the files under the debian directory) for reference.

   i)    Uncompress the tarball; it will unpack into a directory named "2006".
   ii)   Create a directory named cernlib-<version>.orig ("cernlib" being the
         source package name, and <version> the upstream version, including
         any ".dfsg*" suffix).
   iii)  Create a subdirectory "upstream" under cernlib-<version>.orig.
   iv)   For each subdirectory or file inside the directory "2006/src" that
         is mentioned in the $(MY_MODULES) "make" variable at the top of
	 debian/rules, do the following, supposing that the subdirectory
	 or file is named <module>.

         * mkdir -p cernlib-<version>.orig/upstream/2006/src/
	 * cp -a 2006/src/<module> cernlib-<version>.orig/upstream/2006/src/
	 * Delete non-free files: For each file or directory listed in
	   debian/deadpool.txt, if that file or directory appears with the
	   given path relative to cernlib-<version>.orig/upstream/2006/src,
	   delete it.
	 * cd cernlib-<version>.orig/upstream/
	 * tar czf src_<module>.tar.gz 2006
	 * rm -r 2006 # remember, this is under the nascent Debian source
	              # tree, NOT in your main scratch directory
         * Change back up to your scratch directory
   
         For any module tarball src_<module>.tar.gz that contains only code
	 identical to that in the last official Debian orig.tar.gz, you can 
	 just copy in that module tarball from the old unpacked orig.tar.gz
	 to the new source directory, skipping the above bullet points.
	 (Unless, of course, the module tarball needs to be rebuilt because
	 it was just discovered to contain more non-free code.)

   v)    tar cf cernlib_<version>.orig.tar cernlib-<version>.orig
   vi)   gzip -v9 cernlib_<version>.orig.tar

  -- Kevin B. McCarty <kmccarty@debian.org>, Thu, 26 Apr 2007
