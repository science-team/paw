#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

/* stupid program to figure out where ia64 puts stuff */

extern int errno;
int output(void) { return 5; }
int foo;
int bar = 12;
int baz[100];

int main(void)
{
	int foo2;
	int bar2 = 12;
	static int bar3 = 5;
	char baz2[100];
	static char baz3[100] = { 0, };
	char * m;

	m = malloc(5);
	
	printf("shared lib function (malloc) = %p\n", (void *)malloc);
	printf("shared lib variable (errno) =  %p\n\n", (void *)&errno);
	printf("local function (output) =      %p\n", (void *)output);
	printf("local function (main) =        %p\n", (void *)main);
	printf("constant string (\"str\") =      %p\n\n", (void *)"str");
	printf("static variable (foo) =        %p\n", (void *)&foo);
	printf("constant (bar) =               %p\n", (void *)&bar);
	printf("static array (baz) =           %p\n", (void *)baz);
	printf("static local variable (bar3) = %p\n", (void *)&bar3);
	printf("static local array (baz3) =    %p\n", (void *)baz3);
	printf("memory on heap (m) =           %p\n\n", (void *)m);
	// I thought the stack on ia64 was supposed to be 0x8... up to 0xa...
	// why aren't the following found at 0x9fff... ?
	printf("automatic variable (foo2) =    %p\n", (void *)&foo2);
	printf("const auto variable (bar2) =   %p\n", (void *)&bar2);
	printf("automatic array (baz2) =       %p\n", (void *)baz2);
	
	return 0;
}

