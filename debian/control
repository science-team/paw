Source: paw
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 11),
               cernlib-base-dev,
               patch,
               mawk | gawk,
               xutils-dev,
               gfortran,
               cfortran,
               x11proto-core-dev,
               libxt-dev,
               libx11-dev,
               libmotif-dev,
               libxaw7-dev,
               libxbae-dev,
               libblas-dev,
               liblapack-dev,
               libmathlib2-dev,
               libkernlib1-dev,
               libpacklib1-dev,
               libgraflib1-dev,
               libgrafx11-1-dev,
               libpacklib-lesstif1-dev,
               kuipc
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/science-team/paw
Vcs-Git: https://salsa.debian.org/science-team/paw.git
Homepage: http://paw.web.cern.ch/paw/
Rules-Requires-Root: no

Package: libpawlib-lesstif3-dev
Architecture: any
Section: libdevel
Depends: cernlib-base-dev,
         libpawlib2-dev (= ${binary:Version}),
         libpacklib-lesstif1-dev,
         libmotif-dev,
         libxbae-dev,
         libxaw7-dev,
         libpawlib-lesstif3-gfortran (= ${binary:Version}),
         ${misc:Depends}
Breaks: libpaw1-dev (<= 2004.11.04.dfsg-0sarge1),
        libpawlib2-lesstif-dev (<= 1:2.14.04.dfsg-1)
Replaces: libpaw1-dev,
          libpawlib2-lesstif-dev
Description: CERNLIB PAW library (Lesstif-dependent part - development files)
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This package includes the Lesstif-dependent parts of the library pawlib,
 required by the CERNLIB application Paw++, in a static version.  The remainder
 of pawlib is provided in the package libpawlib2-dev.
 .
 The COMIS portion of pawlib is functional on 64-bit machines only when
 statically linked.

Package: libpawlib-lesstif3-gfortran
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: CERNLIB PAW library (Lesstif-dependent part)
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This package includes the Lesstif-dependent parts of the library pawlib,
 required by the CERNLIB application Paw++.  Note that in order to compile and
 link programs against this library, you must also install the
 libpawlib-lesstif3-dev package.  The remainder of pawlib is provided in the
 package libpawlib2-gfortran.
 .
 The COMIS portion of pawlib is functional on 64-bit machines only when
 statically linked.

Package: libpawlib2-dev
Architecture: any
Section: libdevel
Depends: cernlib-base-dev,
         libgraflib1-dev,
         libmathlib2-dev,
         libpawlib2-gfortran (= ${binary:Version}),
         ${misc:Depends}
Suggests: libpawlib-lesstif3-dev
Breaks: libpaw1-dev (<= 2004.11.04.dfsg-0sarge1)
Replaces: libpaw1-dev
Description: CERNLIB PAW library - portion without Lesstif (development files)
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This package includes a static version of (most of) the library pawlib,
 required by the CERNLIB application PAW.  In addition to the internal workings
 of PAW, pawlib also provides the COMIS FORTRAN interpreter and the SIGMA array
 manipulation language.  The Lesstif-dependent parts of the library are
 provided in the libpawlib-lesstif3-dev package.
 .
 The COMIS portion of this library is functional on 64-bit machines only
 when statically linked.

Package: libpawlib2-gfortran
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         gfortran,
         netbase,
         ${misc:Depends}
Recommends: libc6-dev | libc-dev
Pre-Depends: ${misc:Pre-Depends}
Description: CERNLIB PAW library - portion without Lesstif dependencies
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This package includes (most of) the library pawlib, required by the CERNLIB
 application PAW.  In addition to the internal workings of PAW, pawlib
 also provides the COMIS FORTRAN interpreter and the SIGMA array
 manipulation language.  The Lesstif-dependent parts of the library are
 provided in the libpawlib-lesstif3-gfortran package.
 .
 Note that in order to compile and link programs against this library, you
 must install the libpawlib2-dev and maybe also the libpawlib-lesstif3-dev
 package(s).
 .
 The COMIS portion of this library is functional on 64-bit machines only
 when statically linked.

Package: paw++
Architecture: any
Depends: ${shlibs:Depends},
         paw-common,
         kxterm,
         gfortran,
         netbase,
         ${misc:Depends}
Suggests: paw-demos,
          gv
Provides: paw-binary
Description: Physics Analysis Workstation (Lesstif-enhanced version)
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This package includes Paw++, an interactive program for use in analysis and
 graphical presentation.  Paw++ is the same program as PAW (in the "paw"
 package), but with a more user-friendly Motif-based GUI, compiled against
 Lesstif in Debian.
 .
 The program is linked statically against the CERN libraries on
 64-bit architectures in order to function properly, as its design is not
 very 64-bit clean.

Package: paw-common
Architecture: all
Depends: cernlib-base,
         netbase,
         ${misc:Depends}
Recommends: libc6-dev | libc-dev,
            paw | paw-binary
Description: Physics Analysis Workstation (common files)
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This package includes arch-independent files for PAW, an interactive
 program for use in analysis and graphical presentation.  This package
 is useless without also installing the paw or paw++ package.

Package: paw-demos
Architecture: all
Depends: paw | paw-binary,
         ${misc:Depends}
Description: Physics Analysis Workstation examples and tests
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This package includes example scripts for use by PAW or Paw++, and test
 scripts to make sure that the PAW or Paw++ programs behave correctly.  You
 may run the examples and tests with the included paw-demos program.

Package: paw
Architecture: any
Depends: ${shlibs:Depends},
         paw-common,
         gfortran,
         netbase,
         ${misc:Depends}
Suggests: paw-demos,
          gv
Provides: paw-binary
Description: Physics Analysis Workstation - a graphical analysis program
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 PAW is an interactive program providing interactive graphical presentation
 and statistical and mathematical analysis tools.  It is designed to work
 on objects familiar to physicists such as histograms, event files (Ntuples),
 vectors, etc.
 .
 The program is linked statically against the CERN libraries on 64-bit
 architectures in order to function properly, as its design is not
 very 64-bit clean.

